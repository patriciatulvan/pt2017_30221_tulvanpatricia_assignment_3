package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dataAcces.CustomerDAO;
import model.Customer;

public class CustomerBLL {

	public Customer findCustomerById(int idCustomer) {
		Customer c = CustomerDAO.findById(idCustomer);
		if (c == null) {
			throw new NoSuchElementException("The customer with id =" + idCustomer + " was not found!");
		}
		return c;
	}
	
	public int insertCustomer(Customer c) {
		return CustomerDAO.insert(c);
	}
	
	public int deleteCustomer(int id){
		return CustomerDAO.delete(id); 
	}
	
	public ArrayList<Customer> selectAllCustomers(){
		return CustomerDAO.select();
	}
	
	public int updateCustomer(int id, String address, String phone){
		return CustomerDAO.update(id, address, phone);
	}
}
